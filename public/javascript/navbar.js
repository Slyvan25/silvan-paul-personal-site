    var lastScroll = 0;
  
    window.onscroll = function() {
        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop; // Get Current Scroll Value

        if (currentScroll > 120 && lastScroll <= currentScroll){
          lastScroll = currentScroll;
          document.getElementById('navigationbar').style.transform = "translateY(-100%)";
          //document.getElementById('navigationbar').style.opacity='0';                    
        }else if(currentScroll > 120){
          lastScroll = currentScroll;
          document.getElementById('navigationbar').style.transform = "translateY(0%)";
          //document.getElementById('navigationbar').style.opacity='1';
        }
    };
