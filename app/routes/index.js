import Route from '@ember/routing/route';

let homepage = [{
    ExpID: 1,
    title: "Een professioneel design",
    desc: "Natuurlijk is een mooi design belangrijk voor elke site. vandaar dat ik er alles aan doe om de sites die ik maak er zo mooi, professioneel en uniek mogelijk er uit te laten zien.",
    favicon: "style"
},{
    id: 2,
    title: "Zo veilig mogelijk",
    desc: "bij het maken van een site denk ik altijd aan de veiligheid van de site. als de site niet veilig is moet het altijd zo snel mogelijk op gelost worden.",
    favicon: "party_mode"
},{
    id: 3,
    title: "Overzichtelijk",
    desc: "Het belangrijkste punt voor elke site! de gebruiker moet op de site natuurlijk de weg kunnen vinden naar waar hij/zij naar zoekt daarom probeer ik al mijn sites zo overzichtelijk mogelijk te maken.",
    favicon: "face"
}]

export default Route.extend({
    model(){
        return homepage;
    }
});