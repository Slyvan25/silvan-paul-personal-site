import Ember from "ember";

//nav items
export default Ember.Route.extend({
    model(){
        return this.get('store').findAll('navitem');
    }
})
