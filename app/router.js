import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('not-found', {path: '/*path'});
  this.route('projects', {path: "/projects"});
  this.route('index', {path: "/" || "home"});
  this.route('about', {path: "/about"});
  this.route('contact');
  this.route('login');
  this.route('register');

});

export default Router;
