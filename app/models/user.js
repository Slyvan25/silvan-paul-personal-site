import DS from 'ember-data';
import date from 'ember-data/transforms/date';
import store from 'ember-data/store';

export default DS.Model.extend({
  username: DS.attr('string'),
  password: DS.attr('string'),
  isAdmin: DS.attr('boolean', {
    defaultValue: false
  }),
  created: DS.attr('date', {
    defaultValue() {return new date();}
  }),
  email: DS.attr('string'),
  ipadress: DS.attr('string')
});