import DS from 'ember-data';

export default DS.Model.extend({
  title1: DS.attr('string'),
  text1: DS.attr('string'),
  title2: DS.attr('string'),
  text2: DS.attr('string')
});
