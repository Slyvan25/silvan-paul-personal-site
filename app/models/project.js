import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  text: DS.attr('string'),
  url: DS.attr('string'),
  img: DS.attr('string'),
  isVisible: DS.attr('boolean')
});
