import Controller from '@ember/controller';
import Ember from "ember";

export default Controller.extend({
    actions:{
        login(){
            Ember.Logger.info("login button pressed");
        }
    }
});